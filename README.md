
# OpenSCAD Language Support for IntelliJ Platform

This is a plugin for the products based on IntelliJ Platform (Idea, PyCharm, etc.) for the OpenSCAD language. It supports :

* syntax highlighting
* formatting
* code folding support
* structure views
* code completion
* code navigation
* library support
* actions for opening OpenSCAD and exporting model

A color scheme coming close to what the built-in OpenSCAD editor uses is provided and can be selected in the preferences : Editor/Color Scheme/OpenSCAD/Scheme/OpenSCAD.Default.

Formatting options are configurable in the preferences : Editor/Code Style/OpenSCAD.

OpenSCAD executable is searched on standard pathes. If your installation is custom, configure it in the preferences : Languages & Frameworks/OpenSCAD Language. Global libraries are automatically detected.

It is planned to implement the rest of IntelliJ Platform features :
* refactorings;
* intention actions;
* etc.

